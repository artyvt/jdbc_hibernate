package lv.sdacademy.repositories;

import lv.sdacademy.objects.Department;
import lv.sdacademy.objects.Project;
import lv.sdacademy.utils.DataBaseUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    public List<Project> findAll() throws SQLException {
        try (Connection connection = DataBaseUtils.getConnection();
             Statement statement = connection.createStatement();
             PreparedStatement selectAll = connection.prepareStatement("select * from projects");
             ResultSet resultSet = selectAll.executeQuery();) {
            List<Project> projectArrayList = new ArrayList<>();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("projectId");
                String description = resultSet.getString("description");

                Project project = new Project(id, description);
                projectArrayList.add(project);
            }
            return projectArrayList;
        } catch (Exception e) {
            throw new RuntimeException("not connecting to DB");
        }
    }

    public Project findById(Integer id) {
        Project project = new Project();
        try {
            Connection connection = DataBaseUtils.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from projects where projectId = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                project.setProjectId(id);
                project.setDescription(name);
            }

            resultSet.close();
            preparedStatement.close();
            statement.close();
            connection.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }

    public void update(Project project) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("update departments set description = ? where projectId = ?");
        ) {
            preparedStatement.setString(1, project.getDescription());
            preparedStatement.setLong(2, project.getProjectId());

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public void deleteById(Integer projectId) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("delete from projects where projectId = ?");
        ) {
            preparedStatement.setLong(1, projectId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public void save(Project project) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("insert into projects (description)\n" +
                        "value (?);");
        ) {
            preparedStatement.setString(1, project.getDescription());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public List<Project> findByName(String name) {
        try (Connection connection = DataBaseUtils.getConnection();
             PreparedStatement selectByName = connection.prepareStatement("select * from projects where description = ?;");
        ) {
            selectByName.setString(1, name);
            ResultSet resultSet = selectByName.executeQuery();


            List<Project> projectArrayList = new ArrayList<>();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("departmentId");
                String projectDescription = resultSet.getString("name");

                Project project = new Project(id, projectDescription);
                projectArrayList.add(project);
            }
            return projectArrayList;
        } catch (Exception e) {
            throw new RuntimeException("not connecting to DB");
        }
    }

}
