package lv.sdacademy.repositories;

import lv.sdacademy.objects.Department;
import lv.sdacademy.objects.Employee;
import lv.sdacademy.utils.DataBaseUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeRepository {

    public List<Employee> findAll() {
        try (Connection connection = DataBaseUtils.getConnection();
             Statement statement = connection.createStatement();
             PreparedStatement selectAll = connection.prepareStatement("select * from employees");
             ResultSet resultSet = selectAll.executeQuery();) {
            List<Employee> employees = new ArrayList<>();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("employeeId");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                Date dateOfBirth = resultSet.getDate("dateOfBirth");
                String phoneNumber = resultSet.getString("phoneNumber");
                String email = resultSet.getString("email");
                Integer salary = resultSet.getInt("salary");
                Integer departmentId = resultSet.getInt("departmentId");
                Integer managerId = resultSet.getInt("managerId");


                Employee employee = new Employee(
                        id,
                        firstName,
                        lastName,
                        dateOfBirth,
                        phoneNumber,
                        email,
                        salary,
                        departmentId,
                        managerId);

                employees.add(employee);
            }
            return employees;
        } catch (Exception e) {
            throw new RuntimeException("not connecting to DB");
        }
    }

    public Employee findById(Integer id) {
        try {

            Employee employee = new Employee();
            Connection connection = DataBaseUtils.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from employees where employeeId = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer employeeId = resultSet.getInt("employeeId");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                Date dateOfBirth = resultSet.getDate("dateOfBirth");
                String phoneNumber = resultSet.getString("phoneNumber");
                String email = resultSet.getString("email");
                Integer salary = resultSet.getInt("salary");
                Integer departmentId = resultSet.getInt("departmentId");
                Integer managerId = resultSet.getInt("managerId");

                employee = new Employee(

                        firstName,
                        lastName,
                        dateOfBirth,
                        phoneNumber,
                        email,
                        salary,
                        departmentId,
                        managerId);
            }
            return employee;
        } catch (SQLException e) {
            throw new RuntimeException("not connecting to DB");
        }
    }

    public void update(Employee employee) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("update employees set name = ? where employeeId = ?");
        ) {
            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setLong(2, employee.getDepartmentId());

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public void deleteById(Integer employeeId) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("delete from employees where employeeId = ?");
        ) {
            preparedStatement.setLong(1, employeeId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public void save(Employee employee) {
        try (

                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("insert into employees " +
                        "(firstName, lastName, dateOfBirth, phoneNumber, email, salary, departmentId, managerId) " +
                        "values ('?', '?','?', '?', '?', ?, ?, ?);");


        ) {

            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2,employee.getLastName());
            preparedStatement.setDate(3, (Date) employee.getDate());
            preparedStatement.setString(4,employee.getPhoneNumber());
            preparedStatement.setString(5,employee.getEmail());
            preparedStatement.setInt(6, employee.getSalary());
            preparedStatement.setInt(7, employee.getDepartmentId());
            preparedStatement.setInt(8, employee.getManagerId());

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public List<Employee> findByName(String name) {
        try (Connection connection = DataBaseUtils.getConnection();
             PreparedStatement selectByName = connection.prepareStatement("select * from employees where name = ?;");
        ) {
            selectByName.setString(1, name);
            ResultSet resultSet = selectByName.executeQuery();


            List<Employee> employeeList = new ArrayList<>();
            while (resultSet.next()) {
                Integer employeeId = resultSet.getInt("employeeId");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                Date dateOfBirth = resultSet.getDate("dateOfBirth");
                String phoneNumber = resultSet.getString("phoneNumber");
                String email = resultSet.getString("email");
                Integer salary = resultSet.getInt("salary");
                Integer departmentId = resultSet.getInt("departmentId");
                Integer managerId = resultSet.getInt("managerId");

                Employee employee = new Employee(

                        firstName,
                        lastName,
                        dateOfBirth,
                        phoneNumber,
                        email,
                        salary,
                        departmentId,
                        managerId);

                employeeList.add(employee);
            }

            return employeeList;
        } catch (Exception e) {
            throw new RuntimeException("not connecting to DB");
        }
    }

}
