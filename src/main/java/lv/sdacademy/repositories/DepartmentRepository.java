package lv.sdacademy.repositories;

import lv.sdacademy.objects.Department;
import lv.sdacademy.utils.DataBaseUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {

    public List<Department> findAll() throws SQLException {
        try (Connection connection = DataBaseUtils.getConnection();
             Statement statement = connection.createStatement();
             PreparedStatement selectAll = connection.prepareStatement("select * from departments");
             ResultSet resultSet = selectAll.executeQuery();) {
            List<Department> departmentArrayList = new ArrayList<>();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("departmentId");
                String name = resultSet.getString("name");

                Department department = new Department(id, name);
                departmentArrayList.add(department);
            }
            return departmentArrayList;
        } catch (Exception e) {
            throw new RuntimeException("not connecting to DB");
        }
    }

    public Department findById(Integer id) {
        Department department = new Department();
        try {
            Connection connection = DataBaseUtils.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from departments where departmentId = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                department.setDepartmentId(id);
                department.setName(name);
            }

            resultSet.close();
            preparedStatement.close();
            statement.close();
            connection.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return department;
    }

    public void update(Department department) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("update departments set name = ? where departmentId = ?");
        ) {
            preparedStatement.setString(1, department.getName());
            preparedStatement.setLong(2, department.getDepartmentId());

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public void deleteById(Integer departmentId) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("delete from departments where departmentId = ?");
        ) {
            preparedStatement.setLong(1, departmentId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public void save(Department department) {
        try (
                Connection connection = DataBaseUtils.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("insert into departments (name)\n" +
                        "value (?);");
        ) {
            preparedStatement.setString(1, department.getName());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("DB not working!", e);
        }
    }

    public List<Department> findByName(String name) {
        try (Connection connection = DataBaseUtils.getConnection();
             PreparedStatement selectByName = connection.prepareStatement("select * from departments where name = ?;");
        ) {
            selectByName.setString(1, name);
            ResultSet resultSet = selectByName.executeQuery();


            List<Department> departmentArrayList = new ArrayList<>();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("departmentId");
                String departmentName = resultSet.getString("name");

                Department department = new Department(id, departmentName);
                departmentArrayList.add(department);
            }
            return departmentArrayList;
        } catch (Exception e) {
            throw new RuntimeException("not connecting to DB");
        }
    }

}
