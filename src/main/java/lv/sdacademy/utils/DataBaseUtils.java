package lv.sdacademy.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseUtils {
    public static final String DATABASE_HOST = "jdbc:mysql://localhost:3306/jdbc_course_javaRiga10?serverTimezone=UTC";
    public static final String DATABASE_USERNAME = "root";
    public static final String DATABASE_PASSWORD = "A0B1C2_ZZ_$aatttfff"; //change to your password

    private DataBaseUtils() { }

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
        } catch (SQLException ex) {
            throw new IllegalArgumentException("Connection not available!", ex);
        }
    }
}
