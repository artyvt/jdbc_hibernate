package lv.sdacademy;

import lv.sdacademy.objects.Department;
import lv.sdacademy.objects.Employee;
import lv.sdacademy.repositories.DepartmentRepository;
import lv.sdacademy.repositories.EmployeeRepository;
import lv.sdacademy.utils.DataBaseUtils;

import javax.swing.text.DateFormatter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class MainApp {

    public static void main(String[] args) throws Exception {
        DepartmentRepository departmentRepository = new DepartmentRepository();


        List<Department> departments = departmentRepository.findAll();

        departments.forEach(department -> {
            System.out.println(department.toString());
        });


        System.out.println("**********************************");

        Department department = departmentRepository.findById(1);
        System.out.println(department.toString());


        System.out.println("**********************************");
        Department departmentToUpdate = new Department(3, "new Dev team");
        departmentRepository.update(departmentToUpdate);

        departments = departmentRepository.findAll();

        departments.forEach(department1 -> {
            System.out.println(department1.toString());
        });

        System.out.println("**********************************");


        departmentRepository.deleteById(7);

        departments = departmentRepository.findAll();

        departments.forEach(department1 -> {
            System.out.println(department1.toString());
        });

//
//        System.out.println("**********************************");
//        Department newDepartment = new Department("Big Department");
//
//        departmentRepository.save(newDepartment);
//
//        departments = departmentRepository.findAll();
//
//        departments.forEach(department1 -> {
//            System.out.println(department1.toString());
//        });

        System.out.println("**********************************");
        List<Department> departmentList002 = departmentRepository.findByName("HR");

        departmentList002.forEach(department1 -> {
            System.out.println(department1);
        });


        System.out.println("**********************************");

        EmployeeRepository employeeRepository = new EmployeeRepository();
        List<Employee> employeeList = employeeRepository.findAll();

        employeeList.forEach(employee -> {
            System.out.println(employee);
        });

        System.out.println("**********************************");

        Employee employee = employeeRepository.findById(1);
        System.out.println(employee.toString());


        System.out.println("**********************************");

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("1987-06-12");

        Employee employee0002 = new Employee("Robert", "Tomato", date, "37565165 651", "robert@robert.com", 654, 2, 1);
        employeeRepository.save(employee0002);

        employeeList = employeeRepository.findAll();

        employeeList.forEach(employee1 -> {
            System.out.println(employee1);
        });

    }
}
