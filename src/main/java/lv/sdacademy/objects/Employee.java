package lv.sdacademy.objects;


import java.util.Date;

public class Employee {
    private Integer id;
    private String firstName;
    private String lastName;
    private Date date;
    private String phoneNumber;
    private String email;
    private Integer salary;
    private Integer departmentId;
    private Integer managerId;

    public Employee() {
    }

    public Employee(
            Integer id,
            String firstName,
            String lastName,
            Date date,
            String phoneNumber,
            String email,
            Integer salary,
            Integer departmentId,
            Integer managerId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.salary = salary;
        this.departmentId = departmentId;
        this.managerId = managerId;
    }

    public Employee(
            String firstName,
            String lastName,
            Date date,
            String phoneNumber,
            String email,
            Integer salary,
            Integer departmentId,
            Integer managerId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.salary = salary;
        this.departmentId = departmentId;
        this.managerId = managerId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return id +
                " " + firstName +
                " " + lastName +
                " " + date +
                " " + phoneNumber +
                " " + email +
                " " + salary +
                " " + departmentId +
                " " + managerId;
    }
}
